# Changelog
All notable changes to this job will be documented in this file.

## [0.2.0] - 2022-01-28
* Junit kickstart
## [0.1.1] - 2022-01-26
* Added report
## [0.1.0] - 2022-01-24
* Initial version