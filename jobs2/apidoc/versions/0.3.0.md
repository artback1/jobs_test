* Add `APIDOC_SOURCE_PATH` variable
* Add `APIDOC_INCLUDE_FILTER` variable
* Add `APIDOC_EXCLUDE_FILTER` variable
* Add `APIDOC_OPTIONS` variable
* Upgrade `APIDOC_VERSION` default value