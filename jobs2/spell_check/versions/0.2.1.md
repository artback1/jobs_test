* Upgrade `python` image from `3.9` to `3.10-buster`
* Fix `aspell` version from `0.60.7~20110707-6` to `0.60.7~20110707-6+deb10u1`
