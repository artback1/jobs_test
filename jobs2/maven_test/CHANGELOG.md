# Changelog
All notable changes to this job will be documented in this file.

## [0.1.0] - 2021-04-18
* Initial version (renaming of `junit_test` job)
